import numpy as np
import matplotlib.pyplot as plt

class parabolico:

#Vamos a definir los atributos necesarios, tenemos, hi: Altura inicial, v0: Velocidad inicial
#theta:Angulo inicial, t:Tiempo, a:aceleracion

    def __init__(self,hi,v0,theta,t,a):
        self.hi=hi
        self.v0=v0
        self.theta=theta*(np.pi/180)
        self.t=np.arange(0,60,0.1)
        self.a=a
        

#Ahora definimos los metodos necesarios:

    def vx(self): #Velocidad en X
        self.vx=self.v0*np.cos(self.theta)
        return self.vx
    
    def vy(self): #Velocidad en Y
        self.vy=self.v0*(np.sin(self.theta))
        return self.vy
    
    def x(self): #Posicion en X
        return self.vx*(self.t)-(0.5)*(self.a)*((self.t)**2)
        
    def y(self): #Posicion en Y
        g=9.8
        return self.vy*(self.t)-0.5*g*(self.t)**2
        
    def xmax(self): #Alcance maximo
        g=9.8
        self.xmax=(self.v0)**2/g
        return self.xmax
    
    def hmax(self): #Altura maxima
        g=9.8
        return ((self.vy)**2)/2*g

    def tv(self): #Tiempo de vuelo
        g=9.8
        self.tv=(2*(self.vy))/g
        return self.tv

    def xtrayectoria(self): #Grafica
        plt.plot(self.x(),self.t)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("Trayectoria Tiro Parabólico")
        #plt.savefig("Parabolico.png")
        plt.show()




