import numpy as np
import matplotlib.pyplot as plt

#Clase pedida por el profesor para la solución del ejercicio
class PenduloNoLineal:
    
    #Constructor
    def __init__(self, u0, t0, v0, paso, funcion, rango):
        self.y0 = u0                                            #Ángulo inicial
        self.ex = rango                                         #Tiempo máximo
        self.x0 = t0                                            #Tiempo inicial
        self.v0 = v0                                            #Valor inicial de la primera derivada
        self.h = paso                                           #Paso
        self.func = funcion                                     #Función a evaluar
        
    #Método para aplicar RungeKutta    
    def RungeKutta(self):
        self.n = round((self.ex - self.y0)/self.h)  #Definición número de puntos
        x = np.zeros(self.n+1)                 #Inicialización tiempo
        x[0] = self.x0                       #Condición inicial tiempo
        y = np.zeros(self.n+1)                 #Inicialización ángulo
        y[0] = self.y0                       #Condición inicial ángulo
        v = np.zeros(self.n+1)                 #Inicialización derivada
        v[0] = self.v0                       #Condición inicial derivada
        
        #Implementación RungeKutta para una segunda derivada
        for i in range(self.n):
            x[i+1] = self.h+x[i]  
            k1 = self.h*v[i]
            l1 = self.h*self.func(x[i],v[i],y[i])
            k2 = self.h*(v[i] + l1/2)
            l2 = self.h*self.func(x[i] + self.h/2, v[i] + l1/2, y[i] + k1/2)
            k3 = self.h*(v[i] + l2/2)
            l3 = self.h*self.func(x[i] + self.h/2, v[i] + l2/2, y[i] + k2/2)
            k4 = self.h*(v[i] + l3)
            l4 = self.h*self.func(x[i] + self.h, v[i] + l3, y[i] + k3)
            y[i+1] = y[i] + ( k1 + 2*(k2 + k3) + k4)/6
            v[i+1] = v[i] + ( l1 + 2*(l2 + l3) + l4)/6
        return x,v,y
    
    #Método para graficar el desplazamiento angular calculado por RungeKutta contra el tiempo
    def DesplazamientoAngular(self):
        t,v,u = self.RungeKutta()
        plt.plot(t,u)
        plt.xlabel('Tiempo')
        plt.title('Desplazamiento Angular')
        plt.ylabel('Angulo')
        plt.grid(0.2)
        plt.savefig('DesplazamientoAngular.png')
        plt.show()
