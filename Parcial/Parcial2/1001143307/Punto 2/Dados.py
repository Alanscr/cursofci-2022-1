import random
import matplotlib.pyplot as plt

def Probability(Num_dados, Num_expe):
    # Parametros: Num_dados: numero de dados a lanzar
    #             Num_expe: numero de experimentos

    # Lista para agregar la frecuencia de las sumas
    # de los Num_dados lanzados
    P = []
    for _ in range(Num_expe):
        # Lista con los valores de los Num_dados
        D = [random.randint(1, 6) for _ in range(Num_dados)]
        suma = sum(D)
        P.append(suma)
    
    return P

def main():
    Num_expe = 1_000_000
    Num_dados = 4
    bins = 6 * Num_dados - Num_dados + 1
    P = Probability(Num_dados, Num_expe)
    plt.hist(P, bins=bins)
    plt.show()

if __name__=='__main__':
    main()

# No hay diferencia entre los dos casos: tirar los dasos consecutivamente
# y simultaneamente ya que los dados son eventos independientes. 
# por tanto tienen el mismo efecto