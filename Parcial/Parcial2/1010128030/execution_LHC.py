from LHC import colision

if __name__ == '__main__':
    
    col1 = colision(r=0.5,R=100.0,N=1000000)
    col2 = colision(r=0.2,R=100.0,N=1000000)
    col3 = colision(r=2.5,R=100.0,N=1000000)
    col4 = colision(r=40,R=100.0,N=1000000)
    
    print(f"La probabilidad de colisión para r=0.2 es {col2.probabilidad()*100} %\n")
    print(f"La probabilidad de colisión para r=0.5 es {col1.probabilidad()*100} %\n")
    print(f"La probabilidad de colisión para r=2.5 es {col3.probabilidad()*100} %\n")
    print(f"La probabilidad de colisión para r=40 es {col4.probabilidad()*100} %")
