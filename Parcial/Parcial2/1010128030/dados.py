import numpy as np
import matplotlib.pyplot as plt
class dados:
    
    #Método constructor
    def __init__(self,N):
        self.N = N
        
    #Función para tiros simultaneos
    def distribucion_simultaneos(self):
        
        #Sumas posibles
        sumas = [i for i in range(self.N, (6*self.N)+1)]
        
        #Cálculo de tiros simultaneos
        me = np.zeros(len(sumas))
        n = 10000000 #número de tiros
        for j in range(n):
            tiro = np.random.randint(6,size=self.N)+1
            tirosum = sum(tiro)
            
            #Relación de cada tiro con la suma posible
            for i in range(len(sumas)):
                if tirosum == sumas[i]:
                    me[i] += 1
        return sumas, me
    
    #Función para graficas
    def grafica(self, x2,x3,x4,x5,y2,y3,y4,y5,title):
        figure, axis = plt.subplots(2, 2)

        figure.tight_layout(pad=5.0)
        axis[0,0].bar(x2,y2)
        axis[0,0].set_title('Distribución con 2 dados')
        
        axis[0,1].bar(x3,y3)
        axis[0,1].set_title('Distribución con 3 dados')
        
        axis[1,0].bar(x4,y4)
        axis[1,0].set_title('Distribución con 4 dados')
        
        axis[1,1].bar(x5,y5)
        axis[1,1].set_title('Distribución con 5 dados')
        
        figure.supxlabel("macroestado")
        figure.supylabel("microestados")
        figure.suptitle(title)
        plt.savefig(title + '.png')
        plt.show()
                
            
