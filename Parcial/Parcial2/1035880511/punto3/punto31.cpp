#include <iostream>
#include <ctime> 

using namespace std;

// Creación de la clase juego
class Juego{
    // Atributos
    private:
    int maxnum;         // Rango de número para adivinar: (1,maxnum)
    int entrada;        // Número que ingresa el usuario
    int randnum;        // Número aleatorio
   
    // Métodos
    public:
    Juego(int mnum);   
    void AdivinarNumero();

};

// Se defienen los métodos

Juego::Juego(int mnum){
    maxnum = 1000;                  // Este valor puede ser modificado
    
    srand(time(0));
    randnum = 1 + rand() % maxnum ; // genera un aleatorio entre 1 y el número seleccionado

    // Entrada de datos
    cout << "Intruduzca un número entre 1 y " << maxnum << endl;
    cin >> entrada;
}

// Método AdivinadorNumero

void Juego:: AdivinarNumero(){
    while (entrada != randnum)
    {
        if (entrada > randnum)
            cout << "\n" <<"Ingrese un número menor ";
        else
            cout << "\n" <<"Ingrese un número mayor ";
        cin >> entrada; 
    }
    cout << "\n Ganaste! Has adivinado el número" << " " << randnum << "\n";
}

int main()
{
    int maxnum;
    char again;
    do {
        cout << "¡Comenzar Juego!" << " \n" ;
        
        Juego(maxnum).AdivinarNumero();
        cout << "¿Quiere volver a jugar?" << " " << "Presione 'y' para si y 'n' para no"<< "\n";
        cin >> again;

    }while(again == 'y');
    return 0;
}