#include <iostream>
#include <vector>

using namespace std; 

class Field
{
public:
    //Dimensiones del campo
    int x;
    int y;
    int size;
    
    //Arreglo que contiene los parámetros del campo
    std::vector <float> parameters;
    
    //Función de inicialización
    void Dim(int, int);
}u,v,u_prev,v_prev,dens,dens_prev;

void Field::Dim(int xLeng, int yLeng)
{
    x = xLeng;
    y = yLeng;
    size = (x+2) * (y+2);
    
    //Arreglo de parámetros con valores iniciales cero
    parameters.reserve(size);
    for (int i=0; i < size; i++){
        parameters[i] = 0;
    }
}


class Fluid
{
public:
    //Propiedades físicas, tasa difusión y viscosidad'
    float diff;
    float visc;
    int xLeng;
    int yLeng;
    
    void SetValues(int, int, float, float);
    void diffuseD();
    void set_bnd(int, Field);
}P1;

void Fluid::SetValues(int xSize, int ySize, float fludiff, float fluvisc)
{
    xLeng = xSize;
    yLeng = ySize;
    diff = fludiff;
    visc = fluvisc;
    u.Dim(xLeng, yLeng);
    v.Dim(xLeng, yLeng);
    dens.Dim(xLeng, yLeng);
    u_prev.Dim(xLeng, yLeng);
    v_prev.Dim(xLeng, yLeng);
    dens_prev.Dim(xLeng, yLeng);
}

void Fluid::diffuseD()
{
    for (int k = 0; k < 20; k++){
        for (int i = 1; i<= xLeng; i++){
            for (int j=1; j<= yLeng; j++){
                dens.parameters[ i + j * (xLeng + 2)] = (dens_prev.parameters[i + j * (xLeng + 2)] + diff*(dens.parameters[ i -1 + j * (xLeng + 2)] + dens.parameters[ i +1 + j * (xLeng + 2)] + dens.parameters[ i + (j-1) * (xLeng + 2)] + dens.parameters[ i + (j+1) * (xLeng + 2)]))/(1+4*diff);
            }
        }
        set_bnd(0,dens);
    }
}

void Fluid::set_bnd(int b, Field f)
{
    for (int i = 1; i <= xLeng; i++){
        f.parameters[i*(xLeng+2)] = b==1 ? -f.parameters[1 + i*(xLeng+2)] : f.parameters[1+ i*(xLeng+2)];
        f.parameters[xLeng+1 + i*(xLeng+2)] = b==1 ? -f.parameters[xLeng + i*(xLeng+2)] : f.parameters[xLeng + i*(xLeng+2)];
        f.parameters[i] = b==2 ? -f.parameters[i + 1*(xLeng+2)] : f.parameters[i + 1*(xLeng+2)];
        f.parameters[ i + (xLeng+1)*(xLeng+2)] = b==2 ? -f.parameters[i + (xLeng)*(xLeng+2)] : f.parameters[i + (xLeng)*(xLeng+2)];
    }
    f.parameters[0] = 0.5*(f.parameters[1] + f.parameters[(xLeng+2)]);
    f.parameters[(xLeng+1)*(xLeng+2)] = 0.5*(f.parameters[1 + (xLeng+1)*(xLeng+2)] + f.parameters[(xLeng)*(xLeng+2)]);
    f.parameters[(xLeng+1)] = 0.5*(f.parameters[xLeng] + f.parameters[(xLeng+1) + (xLeng+2)]);
    f.parameters[(xLeng+1) + (xLeng+1)*(xLeng+2)] = 0.5*(f.parameters[xLeng + (xLeng+1)*(xLeng+2)] + f.parameters[(xLeng+1) + xLeng*(xLeng+2)]);
}


int main()
{
    P1.SetValues(10,10,0,0);
    cout << u.size << u.parameters.size() << endl;
    
    return 0;
}
