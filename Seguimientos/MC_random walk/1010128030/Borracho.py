import random
import numpy as np
import matplotlib.pyplot as plt

#Función para calcular posición del sujeto después de n pasos
def Sujeto(n):
    #Posición incial
    x, y = 0,0
    
    #Condiciones de movimiento borracho
    for i in range(n):
        j = random.randint(1,100)
        if j<=25:
            y = y+1
        elif 25 < j <=50:
            y = y-1
        elif 50 < j <=75:
            x = x+1
        else:
            x = x-1
        
    return x,y
    
#Función para calcular la probabilidad
def Probabilidad(exp):
    #Arreglo de probabilidades para gráfica e inicialización de variable éxitos
    P = []
    j=0
    
    #Calculo de probabilidad del borracho acabar a dos pasos de la posición incial
    for i in range(1,exp+1):
        x,y = Sujeto(n=10)
        if abs(x) + abs(y) == 2:
            j = j+1
        P.append(j/i)
    return P

#Función principal para graficar el resultado de un total de 1000 experimentos
def main():
    experimentos = 1000
    probabilidad = Probabilidad(experimentos)
    x = np.arange(experimentos,step= 1)
    plt.plot(x, probabilidad)
    plt.grid()
    plt.xlabel('Número de experimentos')
    plt.ylabel('Probabilidad')
    plt.savefig('Probabilidad_Borracho.png')
    plt.show()
    
if __name__ == '__main__':
    main()
        
